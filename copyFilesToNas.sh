#!/bin/bash

# Script might require root permission, depending on system configuration
MOUNT_REQ=true

#PATH
NAS_IP="192.168.0.20"
NAS_HOST_LETTER="P"		# Capital letter
NAS_HOST_PATH="/media/${NAS_HOST_LETTER}"
SRC_PASCAL="/mnt/c/Users/Pascal/Nextcloud/InstantUpload"
SRC_KRISTIN="/mnt/c/Users/Pascal/Nextcloud2/SofortUpload"
DEST_PASCAL="${NAS_HOST_PATH}/Pascal"
DEST_KRISTIN="${NAS_HOST_PATH}/Kristin"

# DYNAMIC DATE
NOW=$(date +%Y-%m)
PREV_YEAR=$(date -d "$NOW-15 last month" '+%Y')
PREV_MONTH=$(date -d "$NOW-15 last month" '+%m')


echo "Copying folders from: 
	Month: $PREV_MONTH 
	Year: $PREV_YEAR"

ping -c 1 ${NAS_IP} > /dev/null
if [ "$?" != 0 ] ; then
	echo "Destination server not available. Sending Wake on Lan..."

	MAC=10:BF:48:8A:07:A3
	Broadcast=255.255.255.255
	PortNumber=4000
	echo -e $(echo $(printf 'f%.0s' {1..12}; printf "$(echo $MAC | sed 's/://g')%.0s" {1..16}) | sed -e 's/../\\x&/g') | nc -w1 -u -b $Broadcast $PortNumber
	echo "Exiting now...."
	exit 1
else
	echo "Destination server available. Continuing with script..."
fi

# mount host drive letter to WSL. Only reqired of WSL/WSL2?
if $MOUNT_REQ ; then
	echo "Mounting remote server"
	sudo mkdir -p ${NAS_HOST_PATH}
	sudo mount -t drvfs ${NAS_HOST_LETTER}: ${NAS_HOST_PATH}
fi


############################# PASCAL #############################
declare -a FOLDERS=("Camera" "WhatsApp Images")

for val in "${FOLDERS[@]}"; do
	echo "Starting: Pascal - ${val}"
	SRC="${SRC_PASCAL}/${val}/${PREV_YEAR}/${PREV_MONTH}"
	DEST="${DEST_PASCAL}/${val}/${PREV_YEAR}/${PREV_MONTH}"
	# echo "${SRC}"
	# echo "${DEST}"
	mkdir -p "${DEST}"
	cp -av "${SRC}/." "${DEST}"
done


############################# KRISTIN #############################
declare -a FOLDERS=("Camera" "WhatsApp Images" "WhatsApp Video" "Screenshots")

for val in "${FOLDERS[@]}"; do
	echo "Starting: Kristin - ${val}"
	SRC="${SRC_KRISTIN}/${val}/${PREV_YEAR}/${PREV_MONTH}"
	DEST="${DEST_KRISTIN}/${val}/${PREV_YEAR}/${PREV_MONTH}"
	# echo "${SRC}"
	# echo "${DEST}"
	mkdir -p "${DEST}"
	cp -av "${SRC}/." "${DEST}"
done

sudo umount ${NAS_HOST_PATH}

echo "****************************************************"
echo "*                Copying successful                *"
echo "****************************************************"
