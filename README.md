# NAS sync



## Getting started

Simple script to sync files from <SRC> to remote <DEST>.
If the remote server can be pinged, push files.

cp -a = same as -dR --preserve=all
    -d = same as --no-dereference and --preserve=links
      ---no-dereference = never follow symbolic links in SOURCE
      --preserve=links = preserve the specified attributes, if possible additional attributes
    -R = copy directories recursively
cp <SRC>/. <DEST>
    /. =Specific cp syntax that allow to copy all files and folders, included hidden ones.


## Improvement
Use rsync directly instead if mounting drive and cp.
